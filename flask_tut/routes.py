from flask import render_template, flash, redirect, url_for
from flask_tut import app
from flask_tut.forms import LoginForm


@app.route('/')
@app.route('/index')
def index():
    user = { 'username': 'Sean McGowan' }
    posts = [
            {
                'author': {'username': 'Sean McGowan'},
                'body': 'What a day outside!'
                },
            { 
                'author': {'username': 'Duke Leto'},
                'body': 'I like beans!'
                }
            ]

    return render_template('index.html', title='Home', user=user, posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)

